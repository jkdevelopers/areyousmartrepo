package com.jksolutions.areyousmart.viewmodels;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.widget.Toast;

import com.jksolutions.areyousmart.BR;
import com.jksolutions.areyousmart.helpers.StringHelper;

/**
 * Created by Junaid AK on 05-Sep-17.
 */

public class VerificationViewModel extends BaseObservable {

    public Context AppContext;
    public String Code;

    public VerificationViewModel(Context context) {
        this.AppContext = context;
        this.Code = "";
    }

    @Bindable
    public String getCode() {
        return this.Code;
    }

    public void setCode(String Code) {
        if (!StringHelper.isNull(Code)) {
            this.Code = Code;
            notifyPropertyChanged(BR.code);
        }
    }

    public void updateCode(CharSequence s, int start, int before, int count) {
        setCode(s.toString());
    }

    public void resendCode(Object object) {
        if (AppContext != null) {
            Toast.makeText(AppContext, "Code Resent", Toast.LENGTH_SHORT).show();
        }
    }

    public void verify(Object object) {
        if (AppContext != null) {
            if (!StringHelper.isNullOrEmpty(Code))
                Toast.makeText(AppContext, "App down for maintenance at the moment", Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(AppContext, "Please enter the code", Toast.LENGTH_SHORT).show();
        }
    }
}
