package com.jksolutions.areyousmart;

import android.app.Activity;
import android.app.Application;

import com.jksolutions.areyousmart.activities.LoginActivity;
import com.jksolutions.areyousmart.activities.SplashActivity;
import com.jksolutions.areyousmart.dagger.components.AppComponent;
import com.jksolutions.areyousmart.dagger.components.DaggerAppComponent;
import com.jksolutions.areyousmart.dagger.modules.AppModule;
import com.jksolutions.areyousmart.dagger.modules.NetModule;
import com.jksolutions.areyousmart.helpers.StringHelper;

/**
 * Created by Junaid AK on 22-Sep-17.
 */

public class ApplicationBase extends Application {
    AppComponent mAppComponent;
    AppModule appModule;
    NetModule netModule;

    @Override
    public void onCreate() {
        super.onCreate();
        updateAppComponent(new SplashActivity());
    }

    public ApplicationBase updateAppComponent(Activity activity) {
        return updateAppComponent(activity, null);
    }

    public ApplicationBase updateAppComponent(Activity activity, String baseUrl) {
        appModule = getAppModule();
        netModule = getNetModule();
        appModule.setCurrentActivity(activity);
        if (!StringHelper.isNullOrEmpty(baseUrl))
            netModule.setBaseUrl(baseUrl);

        mAppComponent = DaggerAppComponent.builder()
                .appModule(appModule)
                .netModule(netModule)
                .build();
        return this;
    }

    public AppComponent getAppComponent() {
        return mAppComponent;
    }

    private AppModule getAppModule() {
        if (appModule == null)
            appModule = new AppModule(getApplicationContext());
        return appModule;
    }

    private NetModule getNetModule() {
        if (netModule == null)
            netModule = new NetModule(getApplicationContext(), "http://jsonplaceholder.typicode.com/");
        return  netModule;
    }

}
