package com.jksolutions.areyousmart.activities;

import android.app.Application;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.jksolutions.areyousmart.ApplicationBase;
import com.jksolutions.areyousmart.R;
import com.jksolutions.areyousmart.databinding.LoginDataBinding;
import com.jksolutions.areyousmart.helpers.ActivityHelper;
import com.jksolutions.areyousmart.viewmodels.LoginViewModel;

import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import retrofit2.Retrofit;

public class LoginActivity extends AppCompatActivity {

    @Inject
    public LoginViewModel LoginVM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityHelper.initActivity(this);
        LoginDataBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        binding.setViewModel(LoginVM);
    }
}
