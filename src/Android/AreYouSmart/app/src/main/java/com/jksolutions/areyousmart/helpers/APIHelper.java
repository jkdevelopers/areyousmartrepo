package com.jksolutions.areyousmart.helpers;

import com.jksolutions.areyousmart.interfaces.IAPIHelper;
import com.jksolutions.areyousmart.interfaces.retrofit.IRestAPI;
import com.jksolutions.areyousmart.models.Post;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Callback;
import retrofit2.Retrofit;

/**
 * Created by Junaid AK on 9/26/2017.
 */
public class APIHelper implements IAPIHelper {
    @Inject
    Retrofit retrofit;

    @Inject
    public APIHelper() {
    }

    public void getPosts(Callback<List<Post>> callback) {
        retrofit.create(IRestAPI.class).getPosts().enqueue(callback);
    }
}
