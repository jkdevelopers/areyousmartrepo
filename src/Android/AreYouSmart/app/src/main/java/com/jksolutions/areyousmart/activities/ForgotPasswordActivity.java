package com.jksolutions.areyousmart.activities;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.jksolutions.areyousmart.R;
import com.jksolutions.areyousmart.databinding.ForgotPasswordDataBinding;
import com.jksolutions.areyousmart.viewmodels.ForgotPasswordViewModel;

import pl.droidsonroids.gif.GifTextView;

public class ForgotPasswordActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        ForgotPasswordDataBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_forgot_password);
        ForgotPasswordViewModel ForgotPasswordViewModel = new ForgotPasswordViewModel(this);
        binding.setViewModel(ForgotPasswordViewModel);
    }
}
