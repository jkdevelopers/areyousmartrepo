package com.jksolutions.areyousmart.viewmodels;

import android.app.Activity;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.text.InputType;
import android.util.Log;
import android.widget.Toast;

import com.jksolutions.areyousmart.BR;
import com.jksolutions.areyousmart.activities.ForgotPasswordActivity;
import com.jksolutions.areyousmart.activities.SignupActivity;
import com.jksolutions.areyousmart.activities.VerificationCodeActivity;
import com.jksolutions.areyousmart.helpers.APICallback;
import com.jksolutions.areyousmart.helpers.APIHelper;
import com.jksolutions.areyousmart.helpers.ActivityHelper;
import com.jksolutions.areyousmart.helpers.AsyncTaskHelper;
import com.jksolutions.areyousmart.helpers.PasswordBoxHelper;
import com.jksolutions.areyousmart.helpers.StringHelper;
import com.jksolutions.areyousmart.interfaces.ILoginViewModel;
import com.jksolutions.areyousmart.models.Post;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Response;

/**
 * Created by Junaid AK on 01-Sep-17.
 */

public class LoginViewModel extends BaseObservable implements ILoginViewModel {

    private static String Username;
    private static String Password;

    private PasswordBoxHelper PasswordHelper;
    private boolean IsPasswordHidden;
    private String ShowPasswordText;

    public String DummyRequestResult;

    @Inject
    public Activity CurrentActivity;

    @Inject
    APIHelper apiHelper;

    @Inject
    public LoginViewModel() {
        initialize();
    }

    private void initialize() {
        PasswordHelper = new PasswordBoxHelper();
        IsPasswordHidden = PasswordHelper.isPasswordHidden();
        ShowPasswordText = PasswordHelper.getShowPasswordText();
    }

    @Bindable
    public String getDummyRequestResult() {
        return DummyRequestResult;
    }

    public void setDummyRequestResult(String dummyRequestResult) {
        DummyRequestResult = dummyRequestResult;
        notifyPropertyChanged(BR.dummyRequestResult);
    }

    @Bindable
    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        if (!StringHelper.isNull(username)) {
            Username = username;
            notifyPropertyChanged(BR.username);
        }
    }

    @Bindable
    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        if (!StringHelper.isNull(password)) {
            Password = password;
            notifyPropertyChanged(BR.password);
        }
    }

    public void onClick(Object object) {
        Toast.makeText(CurrentActivity, "Hello", Toast.LENGTH_SHORT).show();
    }

    public void updateUsername(CharSequence s, int start, int before, int count) {
        setUsername(s.toString());
    }

    public void updatePassword(CharSequence s, int start, int before, int count) {
        setPassword(s.toString());
    }

    @Bindable
    public boolean getIsPasswordHidden() {
        return IsPasswordHidden;
    }

    private void setIsPasswordHidden(boolean isPasswordVisible) {
        IsPasswordHidden = isPasswordVisible;
        notifyPropertyChanged(BR.isPasswordHidden);
    }

    @Bindable
    public String getShowPasswordText() {
        return ShowPasswordText;
    }

    private void setShowPasswordText(String text) {
        ShowPasswordText = text;
        notifyPropertyChanged(BR.showPasswordText);
    }

    public void showPassword(Object object) {
        if (object != null) {
            PasswordHelper.togglePasswordVisibility();
            setShowPasswordText(PasswordHelper.getShowPasswordText());
            setIsPasswordHidden(PasswordHelper.isPasswordHidden());
            PasswordHelper.updatePasswordInputMethod(object);
        }
    }

    public void signUp(Object object) {
        if (CurrentActivity != null) {
            if (!StringHelper.isNullOrEmpty(Username) && !StringHelper.isNullOrEmpty(Password)) {
                if (StringHelper.isValidEmail(Username))
                    ActivityHelper.changeActivity(CurrentActivity, VerificationCodeActivity.class, false);
                else
                    Toast.makeText(CurrentActivity, "Please enter a valid email address", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(CurrentActivity, "Please enter email and pasword", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void login(Object object) {
        if (CurrentActivity != null) {
            if (!StringHelper.isNullOrEmpty(Username) && !StringHelper.isNullOrEmpty(Password)) {

            } else {
                Toast.makeText(CurrentActivity, "Please enter email and pasword", Toast.LENGTH_SHORT).show();
            }

//            apiHelper.getPosts(new APICallback<List<Post>>(CurrentActivity) {
//                @Override
//                public void onSuccess(Response<List<Post>> response) {
//                    setDummyRequestResult(response.body().get(0).getBody());
//                }
//
//                @Override
//                public void onError(Throwable t) {
//                    setDummyRequestResult(t.toString());
//                }
//            });

            new AsyncTaskHelper(CurrentActivity) {
                @Override
                public Object onAsyncTaskDoWork(Object object) {
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    return null;
                }

                @Override
                public void onAsyncTaskCompleted(Object result) {

                }
            }.start();
        }
    }

    public void changeActivity(Object object) {
        if (CurrentActivity != null) {
            ActivityHelper.changeActivity(CurrentActivity, SignupActivity.class, false);
        }
    }

    public void forgotPassword(Object object) {
        if (CurrentActivity != null)
            ActivityHelper.changeActivity(CurrentActivity, ForgotPasswordActivity.class, false);
    }

    public void outsideTouch(Object object) {
        try {
            if (CurrentActivity != null)
                ActivityHelper.hideSoftKeyboard((Activity) CurrentActivity);
        } catch (Exception ex) {
        }
    }


}
