package com.jksolutions.areyousmart.helpers;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.jksolutions.areyousmart.R;
import com.jksolutions.areyousmart.customcontrols.AdvancedProgressBar;

public class CustomLoader {

    private static Activity activity;
    private static RelativeLayout progBarContainer;
    private static AdvancedProgressBar progBar;

    public static void show(Context context) {
        try {
            progBarContainer = getProgBarContainer(context);
            activity = getActivity(context);
            toggleUIInteraction(false);

            if (!viewExistsInActivity())
                addProgBarInActivity();
            else
                toggleProgBarVisibility(true);

            if (progBar != null)
                progBar.start();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void toggleUIInteraction(boolean enable) {
        try {
            if (activity != null) {
                if (enable)
                    activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                else
                    activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
        }
        catch (Exception ex) {}
    }

    public static void hide() {
        toggleProgBarVisibility(false);
        AdvancedProgressBar.stop();
        toggleUIInteraction(true);
    }


    private static void toggleProgBarVisibility(boolean show) {
        try {
            if (progBarContainer != null && activity != null) {
                progBarContainer.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        } catch (Exception ex) {
        }
    }

    private static void addProgBarInActivity() {
        try {
            if (progBarContainer != null && activity != null) {
                activity.addContentView(progBarContainer, getLayoutParams());
            }
        } catch (Exception ex) {
        }
    }

    @NonNull
    private static RelativeLayout.LayoutParams getLayoutParams() {
        try {
            RelativeLayout.LayoutParams lay = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT,
                    RelativeLayout.LayoutParams.MATCH_PARENT);
//            lay.addRule(RelativeLayout.CENTER_IN_PARENT);
//            lay.topMargin = 100;
            return lay;
        } catch (Exception ex) {
        }
        return null;
    }

    private static RelativeLayout getProgBarContainer(Context context) {
        if (progBarContainer == null) {
            View view = LayoutInflater.from(context).inflate(R.layout.custom_progress_dialog, null);
            progBarContainer = (RelativeLayout) view.findViewById(R.id.mainLayout);
            progBar = (AdvancedProgressBar) view.findViewById(R.id.progressBar);
        }
        return progBarContainer;
    }

    private static Activity getActivity(Context context) {
        return (Activity) context;
    }

    private static boolean viewExistsInActivity() {
        return activity.findViewById(R.id.mainLayout) != null;
    }


}