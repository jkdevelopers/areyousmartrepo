package com.jksolutions.areyousmart.activities;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.jksolutions.areyousmart.ApplicationBase;
import com.jksolutions.areyousmart.R;
import com.jksolutions.areyousmart.databinding.LoginDataBinding;
import com.jksolutions.areyousmart.databinding.SignupDataBinding;
import com.jksolutions.areyousmart.helpers.ActivityHelper;
import com.jksolutions.areyousmart.viewmodels.LoginViewModel;
import com.jksolutions.areyousmart.viewmodels.SignupViewModel;

import javax.inject.Inject;

public class SignupActivity extends AppCompatActivity {

    @Inject
    public SignupViewModel SignupVM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityHelper.initActivity(this);
        SignupDataBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_signup);
        binding.setViewModel(SignupVM);
    }
}
