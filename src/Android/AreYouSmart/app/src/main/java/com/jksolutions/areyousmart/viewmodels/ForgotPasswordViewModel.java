package com.jksolutions.areyousmart.viewmodels;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.widget.Toast;

import com.jksolutions.areyousmart.BR;
import com.jksolutions.areyousmart.helpers.StringHelper;

/**
 * Created by Junaid AK on 05-Sep-17.
 */

public class ForgotPasswordViewModel extends BaseObservable {
    public String Username;
    public Context AppContext;

    public ForgotPasswordViewModel(Context context) {
        AppContext = context;
    }

    @Bindable
    public String getUsername() {
        return this.Username;
    }

    public void setUsername(String username) {
        if (!StringHelper.isNull(username)) {
            this.Username = username;
            notifyPropertyChanged(BR.username);
        }
    }

    public void updateUsername(CharSequence s, int start, int before, int count) {
        setUsername(s.toString());
    }

    public void submit(Object object) {
        if (AppContext != null) {
            if (!StringHelper.isNullOrEmpty(Username)) {
                if (StringHelper.isValidEmail(Username)) {
                    Toast.makeText(AppContext, "Please check your email address for instructions", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(AppContext, "Please enter a valid email address", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(AppContext, "Please enter email", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
