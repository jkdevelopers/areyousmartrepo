package com.jksolutions.areyousmart.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;

import com.jksolutions.areyousmart.R;
import com.jksolutions.areyousmart.helpers.ActivityHelper;

import javax.inject.Inject;

import retrofit2.Retrofit;

public class SplashActivity extends AppCompatActivity {
    private static boolean SplashShown = false;
    private static boolean FirstDisplay = true;

    @Inject
    public Retrofit retrofit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //HandleSplash();
        HandleSplashOld();
    }

    private void HandleSplashOld() {
        final Class<?> jumpToActivity = LoginActivity.class;
        //now we can put our logic here if we want to navigate somewhere other than loginActivity

        if (!SplashShown) {
            SplashShown = true;
            setContentView(R.layout.activity_splash);

            try {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ActivityHelper.changeActivity(SplashActivity.this, jumpToActivity);
                    }
                }, 3000);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else
            ActivityHelper.changeActivity(SplashActivity.this, jumpToActivity);
    }

    private void HandleSplash() {
        if (!SplashShown) {
            SplashShown = true;
            setContentView(R.layout.activity_splash);
        }

        Class<?> jumpToActivity = LoginActivity.class;

        //now we can put our logic here if we want to navigate somewhere other than loginActivity

        if (retrofit != null)
            ActivityHelper.changeActivity(SplashActivity.this, jumpToActivity);
        else
            initRetrofitAsync(this, jumpToActivity);
    }

    private void initRetrofitAsync(final SplashActivity activity, final Class<?> jumpToActivity) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                ActivityHelper.initActivity(activity);
                return null;
            }

            protected void onPostExecute(Void result) {
                if (FirstDisplay) {
                    try {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                FirstDisplay = false;

                                if (retrofit != null)
                                    ActivityHelper.changeActivity(SplashActivity.this, jumpToActivity);
                            }
                        }, 1500);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                else {
                    if (retrofit != null)
                        ActivityHelper.changeActivity(SplashActivity.this, jumpToActivity);
                }
            }
        }.execute();
    }

}
