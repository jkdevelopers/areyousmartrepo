package com.jksolutions.areyousmart.dagger.modules;

import android.app.Activity;
import android.content.Context;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Junaid AK on 22-Sep-17.
 */

@Module
public class AppModule {

    private final Context context;
    private Activity currentActivity;

    public AppModule (Context context) {
        this.context = context;
    }

    public AppModule (Context context, Activity activity) {
        this.context = context;
        this.currentActivity = activity;
    }

    @Provides //scope is not necessary for parameters stored within the module
    public Context getContext() {
        return context;
    }

    @Provides
    public Activity getCurrentActivity() {
        return this.currentActivity;
    }

    public void setCurrentActivity(Activity activity) {
        this.currentActivity = activity;
    }

}
