package com.jksolutions.areyousmart.interfaces;

import java.util.Objects;

/**
 * Created by Junaid AK on 9/24/2017.
 */

public interface IAsyncTaskHelper<T> {
    T onAsyncTaskDoWork(T object);
    void onAsyncTaskCompleted(T result);
}
