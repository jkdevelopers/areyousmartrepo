package com.jksolutions.areyousmart.helpers;

/**
 * Created by Junaid AK on 03-Sep-17.
 */

public final class StringHelper {
    static String standardEmailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    public static boolean isNullOrEmpty(String s) {
        return s == null || s.equals("");
    }

    public static boolean isNull(String s) {
        return s == null;
    }

    public static boolean isValidEmail(String email) {
        return email.matches(standardEmailPattern);
    }
}
