package com.jksolutions.areyousmart.customcontrols;

import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Handler;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.util.AttributeSet;
import android.widget.ProgressBar;

import com.jksolutions.areyousmart.R;
import com.jksolutions.areyousmart.activities.SplashActivity;
import com.jksolutions.areyousmart.helpers.ActivityHelper;

/**
 * Created by Junaid AK on 10/6/2017.
 */

public class AdvancedProgressBar extends ProgressBar {

    static int[] tintColors = new int[]
            {
                    R.color.progbar_colorOne,
                    R.color.progbar_colorTwo,
                    R.color.progbar_colorThree,
                    R.color.progbar_colorFour,
                    R.color.progbar_colorFive,
                    R.color.progbar_colorSix,
            };

    static int currentIndex = 0;

    public AdvancedProgressBar(Context context) {
        super(context);
    }

    public AdvancedProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AdvancedProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void start() {
        this.performClick();
        stopRequested = false;
        currentIndex = 0;
        RecursivelyChangeColor();
    }

    public static void stop() {
        stopRequested = true;
    }

    static boolean stopRequested = false;

    private void RecursivelyChangeColor() {
        try {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    ChangeTintColor();
                    if (!stopRequested)
                        RecursivelyChangeColor();
                }
            }, 2000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setProgress(int progress) {
        super.setProgress(progress);
        if (progress == this.getMax()) {
            ChangeTintColor();
        }
    }

    private void ChangeTintColor() {
        try {
            ColorStateList stateList = ColorStateList.valueOf(getResources().getColor(tintColors[currentIndex]));

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                this.setIndeterminateTintList(stateList);
            }

            currentIndex++;
            if (currentIndex == tintColors.length)
                currentIndex = 0;
        } catch (Exception ex) {
        }
    }
}
