package com.jksolutions.areyousmart.dagger.components;

import android.content.Context;

import com.jksolutions.areyousmart.activities.LoginActivity;
import com.jksolutions.areyousmart.activities.SignupActivity;
import com.jksolutions.areyousmart.activities.SplashActivity;
import com.jksolutions.areyousmart.dagger.modules.AppModule;
import com.jksolutions.areyousmart.dagger.modules.NetModule;
import com.jksolutions.areyousmart.dagger.modules.VMModules;

import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;

/**
 * Created by Junaid AK on 22-Sep-17.
 */

@Singleton
@Component(modules = {AppModule.class, NetModule.class, VMModules.class})
public interface AppComponent {

    void inject(LoginActivity loginActivity);

    void inject(SignupActivity signupActivity);

    void inject(SplashActivity splashActivity);
//    void inject(VerificationCodeActivity verificationCodeActivity);
    Context getContext();

    Retrofit getRetrofit();
}
