package com.jksolutions.areyousmart.interfaces.retrofit;

import com.jksolutions.areyousmart.models.Post;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Junaid AK on 9/24/2017.
 */

public interface IRestAPI {

    @GET("/posts")
    Call<List<Post>> getPosts();
}
