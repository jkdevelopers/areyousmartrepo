package com.jksolutions.areyousmart.helpers;

import android.app.Activity;
import android.content.Context;

import com.jksolutions.areyousmart.interfaces.IAPICallback;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Junaid AK on 9/26/2017.
 */

public abstract class APICallback<T> implements Callback<T>, IAPICallback<T> {

    public APICallback(Context context) {
        CustomLoader.show(context);
    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        this.onSuccess(response);
        CustomLoader.hide();
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        this.onError(t);
        CustomLoader.hide();
    }
}
