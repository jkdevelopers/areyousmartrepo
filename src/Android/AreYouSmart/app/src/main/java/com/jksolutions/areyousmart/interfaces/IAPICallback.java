package com.jksolutions.areyousmart.interfaces;

import retrofit2.Response;

/**
 * Created by Junaid AK on 9/26/2017.
 */

public interface IAPICallback<T> {
    void onSuccess(Response<T> response);
    void onError(Throwable t);
}
