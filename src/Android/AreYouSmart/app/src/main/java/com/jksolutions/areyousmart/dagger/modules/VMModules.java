package com.jksolutions.areyousmart.dagger.modules;

import com.jksolutions.areyousmart.interfaces.ILoginViewModel;
import com.jksolutions.areyousmart.interfaces.ISignupViewModel;
import com.jksolutions.areyousmart.viewmodels.LoginViewModel;
import com.jksolutions.areyousmart.viewmodels.SignupViewModel;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Junaid AK on 9/24/2017.
 */

@Module
public class VMModules {
    @Provides
    @Singleton
    ILoginViewModel provideLoginViewModel(LoginViewModel viewModel) {
        return viewModel;
    }

    @Provides
    @Singleton
    ISignupViewModel provideSignupViewModel(SignupViewModel viewModel) {
        return viewModel;
    }
}
