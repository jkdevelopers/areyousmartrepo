package com.jksolutions.areyousmart.helpers;

import android.graphics.Typeface;
import android.text.InputType;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by Junaid AK on 03-Sep-17.
 */

public class PasswordBoxHelper implements InputType {
    private boolean IsPasswordHidden = true;
    private String ShowPasswordText = "Show";

    public boolean isPasswordHidden() {
        return IsPasswordHidden;
    }

    public String getShowPasswordText() {
        return ShowPasswordText;
    }

    public int getInputType() {
        return IsPasswordHidden ? (TYPE_CLASS_TEXT | TYPE_TEXT_VARIATION_PASSWORD) : TYPE_TEXT_VARIATION_VISIBLE_PASSWORD;
    }

    public void togglePasswordVisibility() {
        IsPasswordHidden = !IsPasswordHidden;
        ShowPasswordText = IsPasswordHidden ? "Show" : "Hide";
    }

    public void updatePasswordInputMethod(Object object) {
        try {
            EditText editText = (EditText) ((Button) object).getTag();
            Typeface typeface = editText.getTypeface();
            int length = editText.getText().length();
            editText.setInputType(getInputType());
            editText.setTypeface(typeface);
            editText.setSelection(length);
        } catch (Exception ex) {

        }
    }
}
