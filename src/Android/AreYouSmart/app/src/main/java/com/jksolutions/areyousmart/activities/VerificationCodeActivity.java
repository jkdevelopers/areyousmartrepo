package com.jksolutions.areyousmart.activities;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.jksolutions.areyousmart.R;
import com.jksolutions.areyousmart.databinding.VerificationDataBinding;
import com.jksolutions.areyousmart.viewmodels.VerificationViewModel;

public class VerificationCodeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification_code);

        VerificationDataBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_verification_code);
        VerificationViewModel VerificationViewModel = new VerificationViewModel(this);
        binding.setViewModel(VerificationViewModel);
    }
}
