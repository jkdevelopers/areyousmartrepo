package com.jksolutions.areyousmart.helpers;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.CountDownTimer;

import com.jksolutions.areyousmart.interfaces.IAsyncTaskHelper;

/**
 * Created by Junaid AK on 9/24/2017.
 */

public abstract class AsyncTaskHelper<T> extends AsyncTask<T, T, T> implements IAsyncTaskHelper<T> {
    Context context;

    public AsyncTaskHelper(Context context) {
        this.context = context;
    }

    public AsyncTaskHelper(Context context, int timeoutSeconds) {
        this.context = context;
        initTimeout(timeoutSeconds);
    }

    private void initTimeout(final int timeoutSeconds) {
        final AsyncTaskHelper asyncObject = this;
        new CountDownTimer(timeoutSeconds*1000, timeoutSeconds*1000) {

            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                if (asyncObject.getStatus() == Status.RUNNING) {
                    asyncObject.cancel(false);
                }
            }
        }.start();
    }

    public void start() {
        start(null);
    }

    public void start(T... params) {
//        CustomLoader.show(context);
        GifHelper.show((Activity)context);
        this.execute(params);
    }

    @Override
    protected T doInBackground(T... ts) {
        try {
            return this.onAsyncTaskDoWork(ts == null ? null : ts[0]);
        } catch (Exception ex) {
        }
        return null;
    }

    protected void onPostExecute(T response) {
        try {
//            CustomLoader.hide();
            GifHelper.hide();
            this.onAsyncTaskCompleted(response);
        } catch (Exception ex) {
        }
    }
}
