package com.jksolutions.areyousmart.helpers;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;

import com.jksolutions.areyousmart.ApplicationBase;
import com.jksolutions.areyousmart.activities.LoginActivity;
import com.jksolutions.areyousmart.activities.SignupActivity;
import com.jksolutions.areyousmart.activities.SplashActivity;
import com.jksolutions.areyousmart.dagger.components.AppComponent;

/**
 * Created by Junaid AK on 04-Sep-17.
 */

public final class ActivityHelper {
    public static void changeActivity(Context currentActivity, Class<?> nextActivity) {
        changeActivity(currentActivity, nextActivity, true);
    }

    public static void changeActivity(Context currentActivity, Class<?> nextActivity, boolean clearHistory) {
        try {
            Intent intent = new Intent(currentActivity, nextActivity);
            if (clearHistory)
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            currentActivity.startActivity(intent);
        } catch (Exception ex) {
            Log.i("", ex.getMessage());
        }
    }

    public static void goBack(Context appContext) {
        try {
            ((Activity) appContext).onBackPressed();
        } catch (Exception ex) {
            Log.i("", ex.getMessage());
        }
    }

    public static void hideSoftKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
        catch (Exception ex) {}
    }

    private static AppComponent getAppComponent(Activity activity) {
        return ((ApplicationBase) activity.getApplication()).updateAppComponent(activity).getAppComponent();
    }

    public static void initActivity(SplashActivity activity) {
        AppComponent appComponent = getAppComponent(activity);
        appComponent.inject(activity);
    }

    public static void initActivity(LoginActivity activity) {
        AppComponent appComponent = getAppComponent(activity);
        appComponent.inject(activity);
    }

    public static void initActivity(SignupActivity activity) {
        AppComponent appComponent = getAppComponent(activity);
        appComponent.inject(activity);
    }

//    public static Activity GetActivity(Context context) {
//        ComponentName name = ((ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE)).getRunningTasks(1).get(0).topActivity;
//        return (Activity)name;
//    }

}
