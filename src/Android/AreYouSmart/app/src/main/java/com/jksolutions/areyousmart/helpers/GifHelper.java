package com.jksolutions.areyousmart.helpers;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.jksolutions.areyousmart.R;

import java.io.IOException;

/**
 * Created by Junaid AK on 10/12/2017.
 */

public class GifHelper {
    private static RelativeLayout layout;
    private static Activity currentActivity;

    public static void show(Activity activity) {
        try {
            currentActivity = activity;
            layout = getContainer();
            toggleUIInteraction(false);

            if (!viewExistsInActivity())
                addGifImageInActivity();
            else
                toggleVisibility(true);
        } catch (Exception e) {
        }
    }

    public static void hide() {
        try {
            toggleVisibility(false);
            toggleUIInteraction(true);
        } catch (Exception ex) {
        }
    }

    private static void addGifImageInActivity() {
        try {
            if (layout != null && currentActivity != null)
                currentActivity.addContentView(layout, getLayoutParams());
        } catch (Exception ioe) {

        }
    }

    @NonNull
    private static RelativeLayout.LayoutParams getLayoutParams() {
        try {
            RelativeLayout.LayoutParams lay = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT,
                    RelativeLayout.LayoutParams.MATCH_PARENT);
            return lay;
        } catch (Exception ex) {
        }
        return null;
    }

    private static void toggleVisibility(boolean show) {
        try {
            if (layout != null && currentActivity != null) {
                layout.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        } catch (Exception ex) {
        }
    }

    private static RelativeLayout getContainer() {
        if (layout == null) {
            View view = LayoutInflater.from(currentActivity).inflate(R.layout.display_gif, null);
            layout = (RelativeLayout) view.findViewById(R.id.mainLayout);
        }
        return layout;
    }

    private static void toggleUIInteraction(boolean enable) {
        try {
            if (currentActivity != null) {
                if (enable)
                    currentActivity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                else
                    currentActivity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
        } catch (Exception ex) {
        }
    }

    private static boolean viewExistsInActivity() {
        return currentActivity.findViewById(R.id.mainLayout) != null;
    }
}
