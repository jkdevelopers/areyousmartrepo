package com.jksolutions.areyousmart.viewmodels;

import android.app.Activity;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.widget.Toast;

import com.jksolutions.areyousmart.BR;
import com.jksolutions.areyousmart.activities.ForgotPasswordActivity;
import com.jksolutions.areyousmart.activities.VerificationCodeActivity;
import com.jksolutions.areyousmart.helpers.APIHelper;
import com.jksolutions.areyousmart.helpers.ActivityHelper;
import com.jksolutions.areyousmart.helpers.PasswordBoxHelper;
import com.jksolutions.areyousmart.helpers.StringHelper;
import com.jksolutions.areyousmart.interfaces.ISignupViewModel;

import javax.inject.Inject;

/**
 * Created by Junaid AK on 01-Sep-17.
 */

public class SignupViewModel extends BaseObservable implements ISignupViewModel {

    private static String Username;
    private static String Password;

    private PasswordBoxHelper PasswordHelper;
    private boolean IsPasswordHidden;
    private String ShowPasswordText;

    @Inject
    public Activity CurrentActivity;

    @Inject
    APIHelper apiHelper;

    @Inject
    public SignupViewModel() {
        initialize();
    }

    private void initialize() {
        this.PasswordHelper = new PasswordBoxHelper();
        this.IsPasswordHidden = this.PasswordHelper.isPasswordHidden();
        this.ShowPasswordText = this.PasswordHelper.getShowPasswordText();
    }

    @Bindable
    public String getUsername() {
        return this.Username;
    }

    public void setUsername(String username) {
        if (!StringHelper.isNull(username)) {
            this.Username = username;
            notifyPropertyChanged(BR.username);
        }
    }

    @Bindable
    public String getPassword() {
        return this.Password;
    }

    public void setPassword(String password) {
        if (!StringHelper.isNull(password)) {
            this.Password = password;
            notifyPropertyChanged(BR.password);
        }
    }

    public void onClick(Object object) {
        Toast.makeText(this.CurrentActivity, "Hello", Toast.LENGTH_SHORT).show();
    }

    public void updateUsername(CharSequence s, int start, int before, int count) {
        setUsername(s.toString());
    }

    public void updatePassword(CharSequence s, int start, int before, int count) {
        setPassword(s.toString());
    }

    @Bindable
    public boolean getIsPasswordHidden() {
        return this.IsPasswordHidden;
    }

    private void setIsPasswordHidden(boolean isPasswordVisible) {
        this.IsPasswordHidden = isPasswordVisible;
        notifyPropertyChanged(BR.isPasswordHidden);
    }

    @Bindable
    public String getShowPasswordText() {
        return this.ShowPasswordText;
    }

    private void setShowPasswordText(String text) {
        this.ShowPasswordText = text;
        notifyPropertyChanged(BR.showPasswordText);
    }

    public void showPassword(Object object) {
        if (object != null) {
            PasswordHelper.togglePasswordVisibility();
            setShowPasswordText(PasswordHelper.getShowPasswordText());
            setIsPasswordHidden(PasswordHelper.isPasswordHidden());
            PasswordHelper.updatePasswordInputMethod(object);
        }
    }

    public void signUp(Object object) {
        if (CurrentActivity != null) {
            if (!StringHelper.isNullOrEmpty(Username) && !StringHelper.isNullOrEmpty(Password)) {
                if (StringHelper.isValidEmail(Username))
                    ActivityHelper.changeActivity(CurrentActivity, VerificationCodeActivity.class, false);
                else
                    Toast.makeText(CurrentActivity, "Please enter a valid email address", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(CurrentActivity, "Please enter email and pasword", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void login(Object object) {
        if (CurrentActivity != null) {
            if (!StringHelper.isNullOrEmpty(Username) && !StringHelper.isNullOrEmpty(Password)) {
                if (StringHelper.isValidEmail(Username))
                    Toast.makeText(CurrentActivity, "The provided email address does not match our records", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(CurrentActivity, "Please enter a valid email address", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(CurrentActivity, "Please enter email and pasword", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void changeActivity(Object object) {
        if (this.CurrentActivity != null) {
            ActivityHelper.goBack(this.CurrentActivity);
        }
    }

    public void forgotPassword(Object object) {
        if (this.CurrentActivity != null)
            ActivityHelper.changeActivity(this.CurrentActivity, ForgotPasswordActivity.class, false);
    }

    public void facebook(Object object) {
        if (this.CurrentActivity != null)
            Toast.makeText(CurrentActivity, "Coming soon", Toast.LENGTH_SHORT).show();
    }

    public void outsideTouch(Object object) {
        try {
            if (this.CurrentActivity != null)
                ActivityHelper.hideSoftKeyboard((Activity) this.CurrentActivity);
        } catch (Exception ex) {
        }
    }
}
